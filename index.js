module.exports = function( reqPredicate, middleware ) {
    return async function( req, res, next ) {
        if( await Promise.resolve( reqPredicate( req ) ) )
            middleware( req, res, next );
        else
            next();
    };
};
